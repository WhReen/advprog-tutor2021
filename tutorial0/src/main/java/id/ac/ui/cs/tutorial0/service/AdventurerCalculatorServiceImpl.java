package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        int power;
        if (rawAge<30) {
            power = rawAge*2000;
        } else if (rawAge <50) {
            power = rawAge*2250;
        } else {
            power = rawAge*5000;
        }
        return power;
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    @Override
    public String powerClassifier(int birthYear) {
        int power = countPowerPotensialFromBirthYear(birthYear);
        String pClass;
        if (power > 100000) {
            pClass = "A";
        } else if (power > 20000) {
            pClass = "B";
        } else {
            pClass = "C";
        }
        return pClass;
    }
}
